import Vue from 'vue'
import router from './router'
import App from './views/App.vue'

//import Books from './components/pages/Books'
//import Settings from './components/pages/Settings'
//import router from './router'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

//install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import './assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
