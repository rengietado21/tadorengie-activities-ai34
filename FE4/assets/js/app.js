
var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [7, 10, 9, 12, 13, 6],
      backgroundColor: 'rgba(5, 42, 189, .8)',
    },
    {
      label: 'Returned Books',
      data: [4, 7, 10, 11, 8, 8],
      backgroundColor: 'rgba(99, 146, 196, .8)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [10, 20, 30, 20],
        backgroundColor: [
          'rgb(63, 160, 56)',
          'rgba(248, 168, 3, .8)',
          'rgba(239, 23, 23,.8)',
          'rgba(38, 36, 174, .8)',
        ],
    }],

    labels: [
        'Novel',
        'Biography',
        'Fiction',
        'Horror',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


